@extends("calendar.layout")
@section("content")
<div class="container">
    <div class="row">
        <div class="card-columns">
            @foreach($tablets as $tablet)
                <div class="card blue-grey darken-1">
                    <div class="card-content white-text">
                        <span class="card-title">{{ $tablet->relatedRoom->name }}</span>
                        <p>{{ $tablet->relatedRoom->description }}</p>
                    </div>
                    <div class="card-action">
                        <a href="/tab/{{ $tablet->uuid }}">{{ trans("calendar.goto_calendar") }} {{ $tablet->relatedRoom->name }}</a>
                    </div>
                </div>
            @endforeach
        </div>
    </div>
</div>
@endsection

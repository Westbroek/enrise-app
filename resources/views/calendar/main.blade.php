@extends("calendar.layout")
@section("content")

    @if(Session::has('message'))
        <div class="alert alert-success center" style="display: block">
            <strong class="center">{{ Session::get('message') }}</strong>
        </div>
    @endif

    <div class="container col s12" style="float: none;">
        <div class="calendar light">
            <div class="calendar_plan">
                <div class="cl_plan">
                    <div class="cl_title">{{ $selectedTablet->relatedRoom->name }}</div>
                    <div class="cl_copy">{{ date("l dS F Y") }}</div>
                    <a class="btn-floating btn-large waves-effect waves-light red modal-trigger black" data-target="modalAddEvent">
                        <i class="material-icons">add</i>
                    </a>
                    <button class="btn modal-trigger right" data-target="modalAllEvents">{{ trans("calendar.all_events") }}</button>
                </div>
            </div>
            <div id="calendarItem"></div>
        </div>

    </div>

    <!-- Modal Structure -->
    <div id="modalAddEvent" class="modal modal-event s12">
        <form id="formAddEvent">
            <div class="modal-content">
                <h4 class="">{{ trans("calendar.add_event") }}</h4>

                <div class="alert">
                    <span class="closebtn" onclick="this.parentElement.style.display='none';">&times;</span>
                    {{ trans("calendar.validator.date_exists") }}
                </div>

                <div class="input-field col s12">
                    <label>{{ trans("calendar.name") }}</label>
                    <input required type="text" id="name" name="name" placeholder="{{ trans("calendar.name") }}">
                </div>

                <div class="input-field col s6">
                    <label>{{ trans("calendar.atendees") }}</label>
                    <input required type="text" name="atendees" placeholder="email1@gmail.com email2@gmail.com">
                </div>

                <div class="input-field col s6">
                    <label>{{ trans("calendar.date") }}</label>
                    <input required type="text" name="date" class="datepicker" placeholder="{{ trans("calendar.date") }}">
                </div>

                <div class="input-field col s6">
                    <label>{{ trans("calendar.start_time") }}</label>
                    <input required type="text" name="startTime" class="timepicker" placeholder="{{ trans("calendar.start_time") }}">
                </div>

                <div class="input-field col s6">
                    <label>{{ trans("calendar.end_time") }}</label>
                    <input required type="text" name="endTime" class="timepicker" placeholder="{{ trans("calendar.end_time") }}">
                </div>

                <div class="input-field col s12">
                    <label for="description">{{ trans("calendar.description") }}</label>
                    <textarea id="description" name="description" class="materialize-textarea" placeholder="{{ trans("calendar.description") }}"></textarea>
                </div>
            </div>
            <div class="modal-footer">
                <a href="#!" class="modal-close waves-effect waves-green btn-flat">{{ trans("generic.return") }}</a>
                <button type="submit" id="btn-add-event" class="waves-effect waves-green btn-flat">{{ trans("calendar.add_event") }}</button>
            </div>
        </form>
    </div>
    <!-- End Modal Structure -->

    <!-- All Events Modal -->
    <div id="modalAllEvents" class="modal s12 br-0">
        <div class="col s12 p-0">
            <div class="card br-0 m-0" style="background-color: #f2a900;">
                <div class="card-content br-0 white-text">
                    <span class="card-title">{{ trans("calendar.all_events") }}</span>
                </div>
                <div class="card-tabs">
                    <ul class="tabs tabs-fixed-width">
                        @foreach($eventsForToday as $iteration => $eventsPerRoom)
                            <li class="tab">
                                <a @if($loop->first) class="active" @endif href="#{{ $iteration }}">{{ $roomNames[$iteration] }}</a>
                            </li>
                        @endforeach
                    </ul>
                </div>
                <div class="card-content grey lighten-4">
                    @foreach($eventsForToday as $iteration => $eventsPerRoom)
                        <div class="active" id="{{ $iteration }}">
                            @if(count($eventsPerRoom) == 0)
                                <div>
                                    {{ trans("calendar.no_events") }}
                                </div>
                            @else
                                @foreach($eventsPerRoom as $event)
                                    <div class="event_item">
                                        <div class="ei_Dot"></div>
                                        <div class="ei_Title">
                                            {{ date_format(date_create($event->start->dateTime), "G:i") }} - {{ date_format(date_create($event->end->dateTime), "G:i") }}
                                        </div>
                                        <div class="ei_Copy">{{ $event->summary }}</div>
                                    </div>
                                @endforeach
                            @endif
                        </div>
                    @endforeach
                </div>
            </div>
        </div>
    </div>
    <!-- End All Events -->

    <input type="hidden" id="calendar_id" value="{{ $selectedTablet->calendar_id }}" />

@endsection

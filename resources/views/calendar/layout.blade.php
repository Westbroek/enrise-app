<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
        <meta name="csrf-token" content="{{ csrf_token() }}">

        <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
        <link href="/css/font-awesome.min.css" rel="stylesheet">
        <link href="/css/materialize.css" rel="stylesheet" />
        <link href="/css/calendar.css" rel="stylesheet">
        <title>Enrise meeting room</title>
    </head>
    <body class="row">
        <div class="header-calendar">
            <h1 class="header_title center-align">{{ trans("calendar.title") }}</h1>
        </div>

        @yield("content")

        <script src="/vendors/js/jquery.min.js"></script>
        <script src="https://cdnjs.cloudflare.com/ajax/libs/materialize/1.0.0/js/materialize.min.js"></script>
        <script src="https://unpkg.com/react@16/umd/react.development.js" crossorigin></script>
        <script src="https://unpkg.com/react-dom@16/umd/react-dom.development.js" crossorigin></script>
        <script src="/js/app.js"></script>
        <script src="/js/meetingroomCalendar.js"></script>
    </body>
</html>

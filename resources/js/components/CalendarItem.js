import React from 'react';
import ReactDOM from "react-dom";

function formatDateToHoursMinutes(date) {
    let i = new Date(date);
    return <span>
        {i.getHours() === 0 ? "00" : i.getHours()}:{i.getMinutes() === 0 ? "00" : i.getMinutes()}
    </span>
}

function formatDateToMonthDayYear(date) {
    let i = new Date(date);
    return (i.getMonth() + 1) + "-" + i.getDate() + "-" + i.getFullYear();
}

class CalendarItem extends React.Component {

    constructor(props) {
        super(props);
        this.changeHandler = this.changeHandler.bind(this);
        this.changeHiddenHandler = this.changeHiddenHandler.bind(this);
        this.state = {
            error: null,
            isLoaded: false,
            events: [],
            date: formatDateToMonthDayYear(new Date())
        };
    }

    changeHandler(e) {
        this.setState({
            date: e.target.value,
        });
        console.log(this.state.date);
    }

    changeHiddenHandler() {
        this.forceUpdate();
    }

    getData(day) {
        this.setState({
            isLoaded: false
        });
        fetch((day == null ? "/api/events?calendar_id=" + document.getElementById("calendar_id").value : "/api/events?day=" + this.state.date + "&calendar_id=" + document.getElementById("calendar_id").value))
            .then(res => res.json())
            .then(
                (result) => {
                    this.setState({
                        isLoaded: true,
                        events: result.message
                    });
                },
                (error) => {
                    this.setState({
                        isLoaded: true,
                        error
                    })
                }
            );
    }

    componentDidUpdate(nextProps, nextState) {
        if (nextState.date !==  this.state.date) {
            this.getData(this.state.date);
        }
    }

    componentDidMount() {
        this.getData();
        this.interval = setInterval(this.getData(), 60000);
    }

    componentWillUnmount() {
        clearInterval(this.interval);
    }

    render() {
        const { error, isLoaded, events, date } = this.state;

        if (error) {
            return <div>Error: {error.message}</div>;
        }
        else if (!isLoaded) {
            return <div>Loading</div>;
        }
        else {
            const currentTime = new Date();

            return (
                <div className="calendar_events">
                    <input type="hidden" id="hiddenInput" onChange={this.changeHiddenHandler} />
                    <input type="date" value={date} className="datepicker" id="datepicker-calendar" onChange={this.changeHandler} />
                    { events.length > 0 ?
                        <div>
                            {events.map(event => (
                                <div key={event.googleEvent.id} className={ new Date(event.googleEvent.end.dateTime) >= currentTime ? "event_item" : "event_item op-50" }>
                                    <div className={
                                        new Date(event.googleEvent.start.dateTime) <= currentTime &&
                                        new Date(event.googleEvent.end.dateTime) >= currentTime
                                            ? "ei_Dot dot_active" : "ei_Dot" }>
                                    </div>
                                    <div className="ei_Title">
                                        {formatDateToHoursMinutes(event.googleEvent.start.dateTime)} - {formatDateToHoursMinutes(event.googleEvent.end.dateTime)}
                                    </div>
                                    <div className="ei_Copy">{event.googleEvent.summary}</div>
                                </div>
                            ))}
                        </div>
                        :
                        <div>Not events found this day</div>
                    }
                </div>
            )
        }
    }
}

if (document.getElementById('calendarItem')) {
    ReactDOM.render(<CalendarItem />, document.getElementById('calendarItem'));
}

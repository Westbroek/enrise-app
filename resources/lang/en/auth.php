<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Authentication Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines are used during authentication for various
    | messages that we need to display to the user. You are free to modify
    | these language lines according to your application's requirements.
    |
    */

    'failed' => 'These credentials do not match our records.',
    'throttle' => 'Too many login attempts. Please try again in :seconds seconds.',
    'title' => 'Login',
    'reset_title' => 'Reset my password',
    'email' => 'E-mail address',
    'password' => 'Password',
    'next' => 'Login',
    'password_change_link' => 'Reset password',
    'login_link' => 'Login',
    'reset_button' => 'Reset',
    'reset_sent' => 'A e-mail with instructions to reset your password is sent'

];
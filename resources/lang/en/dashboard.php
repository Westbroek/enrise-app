<?php

return [
    'title' => 'Dashboard',
    'charts' => [
        'todays_events' => 'Todays events',
        'this_weeks_events' => 'This weeks events',
        'events_by_day' => 'Events by day',
    ]
];
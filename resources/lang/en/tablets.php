<?php
return [
    'title' => 'Tablets',
    'add' => 'New tablet',
    'edit' => 'Edit tablet',
    'delete' => 'Delete tablet',
    'room' => 'Room',
    'name' => 'Name',
    'calendar_id' => 'Calendar id',
    'calendar_link' => 'Calendar link',
    'show_calendar' => 'Show calendar',
    'validation' => [
        'calendar_id_required' => 'Calendar id is required'
    ]
];

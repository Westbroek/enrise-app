<?php
return [
    'title' => 'Rooms',
    'add' => 'New room',
    'edit' => 'Edit room',
    'delete' => 'Delete room',
    'models_child_exist' => 'Remove related tablets before deleting this room',
    'name' => 'Name',
    'description' => 'Description',
    'validation' => [
        'name_required' => 'Name is required'
    ]
];
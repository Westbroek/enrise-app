<?php

return [
    "title" => "Enrise meeting rooms",
    "add_event" => "Add event",
    "all_events" => "All events",
    "name" => "Name",
    "atendees" => "Atendees",
    "start_time" => "Start time",
    "end_time" => "End time",
    "date" => "Date",
    "description" => "Description",
    "no_events" => "No events",
    "goto_calendar" => "Go to calendar",
    "validator" => [
        "date_exists" => "A event already exists at this time",
        "date_required" => "Date is required",
        "start_time_required" => "Start time is required",
        "end_time_required" => "End time is required",
        "name_required" => "Name is required"
    ],
    "messages" => [
        "tablet_not_found" => "Tablet not found, using default tablet"
    ]
];

<?php
return [
    'yes' => 'Yes',
    'no' => 'No',
    'return' => 'Return',
    'edit' => 'Edit',
    'agree' => 'Agree',
    'delete' => 'Delete',
    'delete_model' => 'Are you sure you want to delete ',
    'show_label' => 'Rows: ',
    'delete_confirm' => 'Delete confirmation',
    'search' => 'Search:',
    'search_term' => 'To search',
    'sidebar' => [
        'dashboard' => 'Dasboard',
        'rooms' => 'Rooms',
        'tablets' => 'Tablets',
    ],
    'top' => [
        'account' => 'Account',
        'logout' => 'Logout'
    ],
    'validation' => [
        'name_required' => 'Name is required'
    ]
];
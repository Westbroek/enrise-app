<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get("/", "CalendarController@index");
Route::get("/tab", "CalendarController@index");
Route::get("/tab/{tabletId}", "CalendarController@withTabletId");

Auth::routes();

Route::get('/logout', 'Auth\LoginController@logout');

Route::prefix("api")->group(function ()
{
    Route::get("events", "Api\EventController@index");
    Route::get("events/{eventId}", "Api\EventController@show");
    Route::post("events", "Api\EventController@getByTime");
    Route::post("events/add", "Api\EventController@add");
    Route::post("events/{eventId}/edit", "Api\EventController@edit");
    Route::post("events/{eventId}/delete", "Api\EventController@delete");

    Route::get("charts/totalevents", "Api\ChartsApiController@TotalEvents");
    Route::get("charts/eventsbyday", "Api\ChartsApiController@EventsByDay");
});



Route::middleware(["auth"])->group(function ()
{
    Route::get("/manage", "DashboardController@index");

    Route::get('manage/rooms', 'RoomsController@index');
    Route::get('manage/rooms/add', 'RoomsController@add');
    Route::post('manage/rooms/add', 'RoomsController@add');
    Route::get('manage/rooms/{uuid}', 'RoomsController@edit');
    Route::post('manage/rooms/{uuid}', 'RoomsController@edit');
    Route::get('manage/rooms/{uuid}/delete', 'RoomsController@delete');

    Route::get('manage/tablets', 'TabletsController@index');
    Route::get('manage/tablets/add', 'TabletsController@add');
    Route::post('manage/tablets/add', 'TabletsController@add');
    Route::get('manage/tablets/{uuid}', 'TabletsController@edit');
    Route::post('manage/tablets/{uuid}', 'TabletsController@edit');
    Route::get('manage/tablets/{uuid}/delete', 'TabletsController@delete');
});
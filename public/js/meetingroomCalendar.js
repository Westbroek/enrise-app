$(document).ready(function(){

    $(".modal").modal();
    $('.tabs').tabs();
    $(".datepicker").datepicker({
        defaultDate: new Date(),
        setDefaultDate: true,
        firstDay: 1,
        disableWeekends: true
    });
    $(".timepicker").timepicker({
        twelveHour: false
    });
    
    $("#formAddEvent").submit(function (e) {
        e.preventDefault();

        $(".alert").hide();

        $.ajax({
            url: "/api/events",
            method: "POST",
            async: true,
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            },
            data: {
                date: $("input[name=date]").val(),
                startTime: $("input[name=startTime]").val(),
                endTime: $("input[name=endTime]").val(),
                calendarId: $("#calendar_id").val()
            }
        }).done(function (e) {
            if (e.message.length === 0) {
                $.ajax({
                    url: "/api/events/add",
                    method: "POST",
                    async: true,
                    headers: {
                        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                    },
                    data: {
                        name: $("input[name=name]").val(),
                        date: $("input[name=date]").val(),
                        startTime: $("input[name=startTime]").val(),
                        endTime: $("input[name=endTime]").val(),
                        description: $("textarea[name=description]").val(),
                        calendarId: $("#calendar_id").val(),
                        atendees: $("input[name=atendees]").val()
                    }
                }).done(function () {
                    $("#modalAddEvent").modal("close");
                    location.reload();
                }).fail(function (jqXHR, textStatus) {
                    console.log(textStatus);
                });
            }
            else {
                $(".alert").show();
            }
        });
    });

    $("#calendarComponent").attr("data-date", $(this).val());

    $("#datepicker-calendar").change(function () {
        $("#calendarComponent").attr("data-date", $(this).val())
    })

});

$(document).ready(function ($) {

    $('#pagesize').change(function() {
        var url = $(this).data('url');
        url += '&pagesize='+$(this).val();
        document.location.href = url;
    });

    $('.delete-modal').click(function () {
        let content = $(this).data('content');
        if ($(this).data('url') == null || $(this).data('url') === "") {
            $('#confirm-delete').hide();
        }
        else {
            $('#confirm-delete').data('url', $(this).data('url')).show();
        }

        $('#return-delete').text($(this).data("text"));
        $('#delete-body').html(content);
        $('#delete-modal').modal();
        return false;
    });

    $('#confirm-delete').click(function () {
        document.location.href = $(this).data('url');
        return false;
    });

});
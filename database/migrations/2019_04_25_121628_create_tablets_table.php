<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;
use Illuminate\Support\Facades\Schema;

/**
 * Class CreateTabletsTable.
 */
class CreateTabletsTable extends Migration
{
	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('tablets', function(Blueprint $table) {
            $table->increments('id');
            $table->char('uuid',64)->index();
            $table->integer('room_id')->nullable()->unsigned();
            $table->foreign('room_id','rm_tb_id')->references('id')->on('rooms');
            $table->string("name");
            $table->string("calendar_id", 64);
            $table->timestamps();
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('tablets');
	}
}

<?php

use Illuminate\Database\Seeder;

class UsersTableSeeder extends Seeder
{
    /**
     * Run the table seeder
     */
    public function run()
    {
        \App\User::create([
            "uuid" => \Ramsey\Uuid\Uuid::uuid4(),
            "email" => "manager@jwe.nl",
            "name" => "manager",
            "password" => \Illuminate\Support\Facades\Hash::make("manager")
        ]);

        \App\Entities\Room::create([
            "uuid" => \Ramsey\Uuid\Uuid::uuid4(),
            "name" => "Kamer 1",
            "description" => "15 man zaal"
        ]);

        \App\Entities\Room::create([
            "uuid" => \Ramsey\Uuid\Uuid::uuid4(),
            "name" => "Kamer 2",
        ]);

        \App\Entities\Tablet::create([
            "uuid" => \Ramsey\Uuid\Uuid::uuid4(),
            "room_id" => 1,
            "name" => "tablet1",
            "calendar_id" => "upkesr6p61ac18u97ua7obdito@group.calendar.google.com"
        ]);

        \App\Entities\Tablet::create([
            "uuid" => \Ramsey\Uuid\Uuid::uuid4(),
            "room_id" => 2,
            "name" => "tablet2",
            "calendar_id" => "fl3rs4kg7e28lai0c41bjm0ug8@group.calendar.google.com"
        ]);
    }
}
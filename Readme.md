# Enrize Meeting Room App

## Installation
* `composer install`
* `npm install`
* Create schema
* `php artisan migrate:fresh`
* `php artisan db:seed`
* `php artisan serve`
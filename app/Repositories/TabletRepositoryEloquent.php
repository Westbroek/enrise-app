<?php

namespace App\Repositories;

use App\Entities\Tablet;
use Prettus\Repository\Eloquent\BaseRepository;
use Prettus\Repository\Criteria\RequestCriteria;

/**
 * Class TabletsRepositoryEloquent.
 *
 * @package namespace App\Repositories;
 */
class TabletRepositoryEloquent extends BaseRepository implements TabletRepository
{
    protected $fieldSearchable = [
        "name" => "like",
        "relatedRoom.name" => "like"
    ];

    /**
     * Specify Model class name
     *
     * @return string
     */
    public function model()
    {
        return Tablet::class;
    }

    

    /**
     * Boot up the repository, pushing criteria
     */
    public function boot()
    {
        $this->pushCriteria(app(RequestCriteria::class));
    }
    
}

<?php

namespace App\Repositories;

use Prettus\Repository\Contracts\RepositoryInterface;

/**
 * Interface TabletsRepository.
 *
 * @package namespace App\Repositories;
 */
interface TabletRepository extends RepositoryInterface
{
    //
}

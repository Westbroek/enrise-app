<?php

namespace App\Http\Controllers\Api;

use App\Charts\EventsByDay;
use App\Charts\TotalEvents;
use App\Entities\Tablet;
use App\Http\Controllers\Controller;
use Carbon\Carbon;
use DebugBar\DebugBar;
use Illuminate\Http\Request;
use Spatie\GoogleCalendar\Event;

class ChartsApiController extends Controller
{
    /**
     * Return chart data for all the count of events
     *
     * @return string
     */
    public function TotalEvents()
    {
        //Data for charts
        $totalEventsForToday = [];
        $totalEventsForThisWeek = [];

        //Create a new chart
        $eventsCountChart = new TotalEvents();

        //Loop trough all tablets
        foreach (Tablet::all() as $tablet) {

            //Add events for today
            array_push($totalEventsForToday, Event::get(Carbon::today()->startOfDay(),
                Carbon::today()->endOfDay(), [], $tablet->calendar_id)->count());
            //Add events for this week
            array_push($totalEventsForThisWeek, Event::get(Carbon::today()->startOfWeek(),
                Carbon::today()->endOfWeek(), [], $tablet->calendar_id)->count());
        }

        //Bind data to the chart
        $eventsCountChart->dataset(trans("dashboard.charts.todays_events"), "bar",
            $totalEventsForToday)->color("#4897d8")->backgroundColor("#4897d8");
        $eventsCountChart->dataset(trans("dashboard.charts.this_weeks_events"), "bar",
            $totalEventsForThisWeek)->color("#ffdb5c")->backgroundColor("#ffdb5c");

        //Return the api data
        return $eventsCountChart->api();
    }

    /**
     * Return the total events per day
     *
     * @return string
     */
    public function EventsByDay()
    {
        //Create temporary array
        $eventsByDay = [];

        //Create a chart
        $eventsByDayChart = new EventsByDay();

        //Add all the days to search for events to an array
        for ($i = -7; $i <= 7; $i++) {
            $eventsByDay[Carbon::now()->add($i, "days")->format("Y-m-d")] = 0;
        }

        //Get all tablets
        foreach (Tablet::all() as $tablet) {
            //Get all events
            foreach (Event::get(Carbon::now()->startOfDay()->add(-7, "days"), Carbon::now()->endOfDay()->add(7, "days"), [], $tablet->calendar_id) as $event) {
                //Push all data to the array
                $date = Carbon::parse($event->start->dateTime)->format("Y-m-d");
                $eventsByDay[$date] += 1;
            }
        }

        $eventsByDayChart->dataset(trans("dashboard.charts.events_by_day"), "line", array_values($eventsByDay))->options([
            "lineTension" => 0
        ])->color("#ff0000")->backgroundColor("#ff0000")->fill(false);

        //Return the api
        return $eventsByDayChart->api();
    }
}

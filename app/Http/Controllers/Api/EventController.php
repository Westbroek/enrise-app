<?php

namespace App\Http\Controllers\Api;

use App\Charts\EventsByDay;
use App\Entities\Tablet;
use App\Http\Api\ApiResponse;
use App\Http\Controllers\Controller;
use Carbon\Carbon;
use Google_Service_Calendar_Event;
use Illuminate\Http\Request;
use Spatie\GoogleCalendar\Event;

class EventController extends Controller
{
    /**
     * Gets all events for the current day or the given day
     *
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function index(Request $request)
    {
        //Get the current day if its given
        $day = $request->get("day", date("Y-m-d"));
        $events = Event::get(Carbon::parse($day)->startOfDay(), Carbon::parse($day)->endOfDay(), [], $request->get("calendar_id", null));

        return ApiResponse::simple(200, $events);
    }

    /**
     * Get a single event by event id
     *
     * @param Request $request
     * @param $eventId
     * @return \Illuminate\Http\JsonResponse
     */
    public function show(Request $request, $eventId)
    {
        $event = Event::find($eventId);

        return ApiResponse::simple(200, $event);
    }

    /**
     * Gets all event between start and ending time
     *
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function getByTime(Request $request)
    {
        $startTime = Carbon::parse($request->get("date") . $request->get("startTime") . env("DEFAULT_TIME_ZONE"));
        $endTime = Carbon::parse($request->get("date") . " " . $request->get("endTime") . env("DEFAULT_TIME_ZONE"));

        $event = Event::get($startTime, $endTime, [], $request->get("calendarId", null));

        return ApiResponse::simple(200, (!$event ? false : $event));
    }

    /**
     * Adds a event
     *
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function add(Request $request)
    {
        $startTime = Carbon::parse($request->get("date") . $request->get("startTime") . env("DEFAULT_TIME_ZONE"));
        $endTime = Carbon::parse($request->get("date") . " " . $request->get("endTime") . env("DEFAULT_TIME_ZONE"));

        $event = Event::create([
            "name" => $request->get("name", ""),
            "startDateTime" => $startTime,
            "endDateTime" => $endTime,
            "description" => $request->get("description", ""),

        ], $request->get("calendarId", env("GOOGLE_CALENDAR_ID")));

        $atendees = explode(" ", $request->get("atendees", ""));

        foreach ($atendees as $atendee) {
            if (filter_var($atendee, FILTER_VALIDATE_EMAIL)) {
                $event->addAttendee(['email' => $atendee]);
            }
        }

        $event->save();

        return ApiResponse::simple(200, "Event added");
    }

    /**
     * Edit a event on the get parameters
     *
     * @param Request $request
     * @param $eventId
     * @return \Illuminate\Http\JsonResponse
     */
    public function edit(Request $request, $eventId)
    {
        $event = Event::find($eventId)();
        $event->name = $request->get("name");
        $event->startDateTime = Carbon::parse($request->get("startDateTime"));
        $event->endDateTime = Carbon::parse($request->get("endDateTime"));
        $event->calendar_id = $request->get("calendar_id");
        $event->save();

        return ApiResponse::simple(200, "Event edited");
    }

    /**
     * Removes a event
     *
     * @param Request $request
     * @param $eventId
     * @return \Illuminate\Http\JsonResponse
     */
    public function delete(Request $request, $eventId)
    {
        $event = Event::find($eventId);
        $event->delete();

        return ApiResponse::simple(200, "Event deleted");
    }
}
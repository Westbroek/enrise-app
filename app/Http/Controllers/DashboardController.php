<?php

namespace App\Http\Controllers;

use App\Charts\EventsByDay;
use App\Charts\TotalEvents;
use App\Entities\Tablet;
use Carbon\Carbon;
use Illuminate\Support\Facades\URL;

class DashboardController extends Controller
{
    public function index()
    {
        //Label used for chart
        $roomNames = [];
        $days = [];

        for ($i = -7; $i <= 7; $i++) {
            array_push($days, Carbon::now()->add($i, "days")->format("Y-m-d"));
        }

        $eventsCountChart = new TotalEvents();
        $eventsByDayChart = new EventsByDay();

        foreach (Tablet::all() as $tablet) {
            array_push($roomNames, $tablet->relatedRoom->name);
        }

        //Binding the label to charts
        $eventsCountChart->labels = $roomNames;
        $eventsByDayChart->labels = $days;

        $eventsCountChart->load("/api/charts/totalevents");
        $eventsByDayChart->load("/api/charts/eventsbyday");

        return view("manage.dashboard.index", [
            'eventsCountChart' => $eventsCountChart,
            'eventsByDayChart' => $eventsByDayChart
        ]);
    }
}

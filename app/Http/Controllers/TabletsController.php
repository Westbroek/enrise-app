<?php

namespace App\Http\Controllers;

use App\Entities\Room;
use App\Entities\Tablet;
use App\Repositories\RoomRepositoryEloquent;
use App\Repositories\TabletRepositoryEloquent;
use App\Validators\TabletValidator;
use Illuminate\Http\Request;
use Ramsey\Uuid\Uuid;
use App\Validators\NameValidator;

class TabletsController extends Controller
{
    /**
     * @var
     */
    private $customer;

    /**
     * @var int
     */
    private $limit = 25;

    /**
     * @var RoomRepositoryEloquent
     */
    private $repo;

    /**
     * EventsController constructor.
     */
    public function __construct()
    {
        $this->repo = new TabletRepositoryEloquent(app());
    }

    /**
     * @param Request $request
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function index(Request $request)
    {
        $itemsPerPage = $request->get('pagesize', $this->limit);

        $this->customer = auth()->user()->customer;
        $message = session()->get('message', '');
        $error = session()->get('error', '');
        $tablets = $this->repo->orderBy("name", "asc")->paginate($itemsPerPage);

        return view('manage.tablets.index', [
            'message' => $message,
            'error' => $error,
            'tablets' => $tablets,
            'itemsPerPage' => $itemsPerPage,
            'search' => $request->get('search', '')
        ]);
    }

    /**
     * @param Request $request
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector|\Illuminate\View\View
     * @throws \Exception
     */
    public function add(Request $request)
    {
        $message = session()->get('message', '');
        $error = session()->get('error', '');

        if ($request->isMethod('post')) {

            $validator = new TabletValidator($request->all());
            $result = $validator->isValid();

            if ($result === true)
            {
                $tablet = new Tablet();
                $tablet->uuid = Uuid::uuid4();
                $tablet->room_id = $request->get('room_id');
                $tablet->name = $request->get('name');
                $tablet->calendar_id = $request->get('calendar_id');
                $tablet->save();
                return redirect('/manage/tablets');
            }

            $error = $result->messages();
        }

        return view('manage.tablets.add', [
            'message' => $message,
            'error' => $error,
            'tablet' => $request->all(),
            'rooms' => Room::all()
        ]);
    }

    /**
     * @param Request $request
     * @param $uuid
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector|\Illuminate\View\View
     */
    public function edit(Request $request, $uuid)
    {
        $tablet = Tablet::where('uuid', '=', $uuid)->first();

        $message = session()->get('message', '');
        $error = session()->get('error', '');

        if ($request->isMethod('post')) {

            $validator = new TabletValidator($request->all());
            $result = $validator->isValid();

            if ($result === true)
            {
                $tablet->room_id = $request->get('room_id');
                $tablet->name = $request->get('name');
                $tablet->calendar_id = $request->get('calendar_id');
                $tablet->save();
                return redirect('/manage/tablets');
            }

            $tablet = $request->all();
            $error = $result->messages();
        }

        return view('manage.tablets.edit', [
            'message' => $message,
            'error' => $error,
            'tablet' => $tablet,
            'rooms' => Room::all()
        ]);
    }

    /**
     * @param Request $request
     * @param $uuid
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function delete(Request $request, $uuid)
    {
        $tablet = Tablet::where("uuid", $uuid)->first();
        $tablet->delete();

        return redirect('/manage/tablets');
    }
}

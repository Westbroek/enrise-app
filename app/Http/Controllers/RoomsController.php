<?php

namespace App\Http\Controllers;

use App\Entities\Room;
use App\Repositories\RoomRepositoryEloquent;
use Illuminate\Http\Request;
use Ramsey\Uuid\Uuid;
use App\Validators\NameValidator;

class RoomsController extends Controller
{
    /**
     * @var
     */
    private $customer;

    /**
     * @var int
     */
    private $limit = 25;

    /**
     * @var RoomRepositoryEloquent
     */
    private $repo;

    /**
     * EventsController constructor.
     */
    public function __construct()
    {
        $this->repo = new RoomRepositoryEloquent(app());
    }

    /**
     * @param Request $request
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function index(Request $request)
    {
        $itemsPerPage = $request->get('pagesize', $this->limit);

        $this->customer = auth()->user()->customer;
        $message = session()->get('message', '');
        $error = session()->get('error', '');
        $rooms = $this->repo->orderBy("name", "asc")->paginate($itemsPerPage);

        return view('manage.rooms.index', [
            'message' => $message,
            'error' => $error,
            'rooms' => $rooms,
            'itemsPerPage' => $itemsPerPage,
            'search' => $request->get('search', '')
        ]);
    }

    /**
     * @param Request $request
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector|\Illuminate\View\View
     * @throws \Exception
     */
    public function add(Request $request)
    {
        $message = session()->get('message', '');
        $error = session()->get('error', '');

        if ($request->isMethod('post')) {

            $validator = new NameValidator($request->all());
            $result = $validator->isValid();

            if ($result === true)
            {
                $room = new Room();
                $room->uuid = Uuid::uuid4();
                $room->name = $request->get('name');
                $room->description = $request->get('description');
                $room->save();
                return redirect('/manage/rooms');
            }

            $error = $result->messages();
        }

        return view('manage.rooms.add', [
            'message' => $message,
            'error' => $error,
            'room' => $request->all()
        ]);
    }

    /**
     * @param Request $request
     * @param $uuid
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector|\Illuminate\View\View
     */
    public function edit(Request $request, $uuid)
    {
        $room = Room::where('uuid', '=', $uuid)->first();

        $message = session()->get('message', '');
        $error = session()->get('error', '');

        if ($request->isMethod('post')) {

            $validator = new NameValidator($request->all());
            $result = $validator->isValid();

            if ($result === true)
            {
                $room->name = $request->get('name');
                $room->description = $request->get('description');
                $room->save();
                return redirect('/manage/rooms');
            }
            $room = $request->all();
            $error = $result->messages();
        }

        return view('manage.rooms.edit', [
            'message' => $message,
            'error' => $error,
            'room' => $room,
        ]);
    }

    /**
     * @param Request $request
     * @param $uuid
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function delete(Request $request, $uuid)
    {
        $room = Room::where("uuid", $uuid)->first();
        $room->delete();

        return redirect('/manage/rooms');
    }
}

<?php

namespace App\Http\Controllers;

use App\Entities\Tablet;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Session;
use Spatie\GoogleCalendar\Event;

class CalendarController extends Controller
{
    /**
     * Shows the calendar for the first tablet in the database
     *
     * @param Request $request
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function index(Request $request)
    {
        $tablets = Tablet::all();

        return view("calendar.selecttablet", [
            "tablets" => $tablets
        ]);
    }

    /**
     * Shows the tablet for the given tablet in the database
     *
     * @param Request $request
     * @param $tabletId
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function withTabletId(Request $request, $tabletId)
    {
        $roomNames = [];
        $eventsForToday = [];

        $selectedTablet = Tablet::where("uuid", $tabletId)->first();

        if (!$selectedTablet) {
            $selectedTablet = Tablet::all()->first();
            Session::flash("message", trans("calendar.messages.tablet_not_found"));
        }

        foreach (Tablet::all() as $tablet) {
            array_push($roomNames, $tablet->relatedRoom->name);
            array_push($eventsForToday, Event::get(Carbon::now()->startOfDay(), Carbon::now()->endOfDay(), [], $tablet->calendar_id));
        }

        return view("calendar.main", [
            "selectedTablet" => $selectedTablet,
            "roomNames" => $roomNames,
            "eventsForToday" => $eventsForToday,
        ]);
    }
}

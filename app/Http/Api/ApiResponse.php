<?php

namespace App\Http\Api;

use Illuminate\Http\JsonResponse;

class ApiResponse
{

    /**
     * @param $statusCode
     * @param $message
     * @param array $data
     * @return JsonResponse
     */
    public static function simple($statusCode, $message, $data = []): JsonResponse
    {
        $data = [
            'success' => true,
            'message' => $message,
            'data' => $data
        ];
        return response()->json(
            $data,
            $statusCode
        );
    }

    /**
     * @param int $statusCode HTTP Status code
     * @param string $errorCode Unique $errorCode Unique error code
     * @param string $message Error help message
     * @param array $errors Errors
     *
     * @return JsonResponse
     */
    public static function error($statusCode, $errorCode, $message = '', $errors = []): JsonResponse
    {
        $data = [
            'success' => false,
            'error' => $message,
            'error_code' => $errorCode
        ];
        if (count($errors) > 0) {
            $data['errors'] = $errors;
        }

        return response()->json(
            $data,
            $statusCode
        );
    }
}

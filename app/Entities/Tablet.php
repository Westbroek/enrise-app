<?php

namespace App\Entities;

use Illuminate\Database\Eloquent\Model;
use Prettus\Repository\Contracts\Transformable;
use Prettus\Repository\Traits\TransformableTrait;

/**
 * Class Tablets.
 *
 * @package namespace App\Entities;
 */
class Tablet extends Model implements Transformable
{
    use TransformableTrait;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [];

    public function relatedRoom()
    {
        return $this->belongsTo(Room::class, "room_id", "");
    }

}

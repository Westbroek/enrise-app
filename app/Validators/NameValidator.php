<?php

namespace App\Validators;

use Illuminate\Support\Facades\Validator;

class NameValidator
{

    /**
     * @var
     */
    private $values;

    public function __construct($values)
    {
        $this->values = $values;
    }


    /**
     * @return bool
     */
    public function isValid()
    {
        $val = Validator::make($this->values, [
            'name' => 'required'
        ], [
            'name.required' => trans('generic.validation.name_required'),
        ]);
        if ($val->fails()) {
            return $val->errors();
        }
        return true;
    }
}

<?php

namespace App\Validators;

use Illuminate\Support\Facades\Validator;

class TabletValidator
{

    /**
     * @var
     */
    private $values;

    public function __construct($values)
    {
        $this->values = $values;
    }


    /**
     * @return bool
     */
    public function isValid()
    {
        $val = Validator::make($this->values, [
            'name' => 'required',
            'calendar_id' => 'required'
        ], [
            'name.required' => trans('generic.validation.name_required'),
            'calendar_id.required' => trans('tablet.validation.calendar_id_required'),
        ]);
        if ($val->fails()) {
            return $val->errors();
        }
        return true;
    }
}